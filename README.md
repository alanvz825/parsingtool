# ParsingTool

Console parsing tool that takes a config file in the bin folder in the Parsing Tool project and parse into a usable object in C#.

## Executing the console application

This project consist in a console application and a Class library. To execute the application, it requires to pull the main branch, and in the folder root, execute the following command in console: 

```cmd
cd ParsingTool
dotnet publish
```

Executable will be in the following folder: 

`\ParsingTool\bin\Debug\netcoreapp3.1\publish\`



