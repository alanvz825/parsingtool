using System;
using System.Collections.Generic;
using System.IO;
using ParsingClassLibrary;
using System.Linq;

namespace ParsingTool
{
    class Program
    {
        static void Main(string[] args)
        {
            //First it will find the config file from the folder project
            string path = @"src\\Config.txt";

            //Read the file as an array, that every element in the array represent a line in the Config file
            string[] strConfigFile = File.ReadAllLines(path);

            //make config file List
            List<Parsing> lstConfigList = new List<Parsing>();
            
            foreach (string strLine in strConfigFile)
            {
                //Ignore comments
                if ( !strLine.StartsWith('#') && strLine.Contains('='))
                {
                    Parsing Element = new Parsing(strLine);

                    //Add into the new list.
                    lstConfigList.Add(Element);
                }
            }

            bool bolMenuExit = true;

            do
            {
                Console.WriteLine($"Please input the configuration key to retrieve the key value: ");

                foreach (Parsing item in lstConfigList)
                {
                    Console.WriteLine("| {0:-10} |", item.Key);
                }

                string strUserInput = Console.ReadLine();
                //Console.Clear();

                bool isItemFound = false;
                foreach (Parsing item in lstConfigList)
                {
                    if (item.Key.ToLower() == strUserInput)
                    {
                        Console.WriteLine($"| Key: {item.Key} | Value: {item.Value.ToString()} ");
                        isItemFound = true;
                    }
                }
                if (!isItemFound)
                {
                    Console.WriteLine("Key value did not found in config file.");
                }


                
                Console.WriteLine("Look into another value? Y/N");

                string strRetry = Console.ReadLine();

                if (strRetry.Substring(0,1).ToLower() ==  "n" || strRetry.Substring(0, 1).ToLower() == "x")
                {
                    bolMenuExit = false;
                }

                //Console.Clear();

            } while (bolMenuExit);


        }
    }
}

