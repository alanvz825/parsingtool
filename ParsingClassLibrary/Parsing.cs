﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ParsingClassLibrary
{
    public class Parsing
    {

        private string _key;

        public string Key
        {
            get { return _key; }
            set { _key = value; }
        }

        private object _value;

        public object Value
        {
            get { return _value; }
            set
            {
                //Numeric
                double dblValue = 0;
                bool bolIsNumeric = double.TryParse(value.ToString(), out dblValue);
                if (bolIsNumeric)
                    _value = dblValue;

                //Boolean
                else if (value.ToString().ToLower() == "on" || value.ToString().ToLower() == "yes" || value.ToString().ToLower() == "true")
                    _value = true;
                else if (value.ToString().ToLower() == "off" || value.ToString().ToLower() == "no" || value.ToString().ToLower() == "false")
                    _value = false;
                else
                    _value = value;

                
            }
        }

        /// <summary>
        /// Constructor for creating Parsing Objects.
        /// </summary>
        /// <param name="strElement"> Receives the line of code that have specific configuration. </param>
        public Parsing( string strElement )
        {
            this.Key = strElement.Substring(0, strElement.IndexOf('=')).Trim();
            this.Value = strElement.Substring(strElement.IndexOf('=') + 1, strElement.Length - strElement .IndexOf('=') - 1).Trim();
        }

        /// <summary>
        /// Function that generates a list of Configuration key values. If the value haves comments(#) or it do not have the correct format, it will ignore the line.
        /// </summary>
        /// <param name="strLine">Parameter that takes the lines of the configuration file and convert into a parsing element.</param>
        /// <returns>Returns a list of Parsing objects with the parameters retrieved from the configuration file.</returns>
        public static List<Parsing> GetListOfConfigurations(string[] strConfigFile)
        {
            List<Parsing> lstConfigList = new List<Parsing>();

            foreach (string strLine in strConfigFile)
            {
                //Ignore comments
                if (!strLine.StartsWith('#') && strLine.Contains('='))
                {
                    Parsing Element = new Parsing(strLine);

                    //Add into the new list.
                    lstConfigList.Add(Element);
                }
            }
            return lstConfigList;
        }

    }
}
